function NhanVien(_taikhoan ,
_hovaTen ,
_email ,
_matKhau ,
_ngayLam ,
_luongCB ,
_chucVu ,
_gioLam ) {
    this.taikhoan = _taikhoan , 
    this.hovaTen = _hovaTen , 
    this.email = _email , 
    this.matKhau = _matKhau , 
    this.ngayLam = _ngayLam , 
    this.luongCB = _luongCB , 
    this.chucVu = _chucVu ,
    this.gioLam = _gioLam , 
    this.tinhLuong = function() {
        return this.chucVu == "Sếp" ? this.luongCB * 3 : this.chucVu == "Trưởng phòng" ? this.luongCB *2 : this.luongCB ; 
    }
    ,
    this.xepLoai = function() {
        return this.gioLam >= 196 ? "Xuất sắc" : this.gioLam >=176 ? "Giỏi" : this.gioLam >= 160 ? "Khá" : "Trung bình" ; 
    }


}