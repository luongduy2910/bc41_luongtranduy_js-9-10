function kiemTraTrung(arrNV) {
    var tk = document.getElementById('tknv').value ; 
    var flag = true ; 
    var viTri = arrNV.findIndex(function(item){
        return item.taikhoan == tk ;
    }) ; 
    if (viTri != -1) {
        document.getElementById('tbTKNV').innerHTML = "Mã sinh viên đã tồn tại" ; 
        flag = false ;
    }else {
        document.getElementById('tbTKNV').innerHTML = "" ; 
        flag = true ;
    }
    return flag ;
}

function kiemTraDoDai(min , max ) {
    var tk = document.getElementById('tknv').value ; 
    var flag = true ; 
    var length = tk.length ;
    if (length < min || length > max) {
        document.getElementById('tbTKNV').innerHTML = `Vui lòng nhập ${min} đến ${max} ký tự` ; 
        flag = false ;
    }else {
        document.getElementById('tbTKNV').innerHTML = "" ; 
        flag = true ;
    }
    return flag ;
}

function kiemTraSo() {
    var flag = true ; 
    var tk = document.getElementById('tknv').value ; 
    if (!tk.match(/^[0-9]+$/)) {
        document.getElementById('tbTKNV').innerHTML = `Mã sinh viên chỉ bao gồm số` ; 
        flag = false ;
    }else {
        document.getElementById('tbTKNV').innerHTML = "" ;
        flag = true ;
    }
    return flag ;
}

function kiemTraTen() {
    var flag = true ; 
    var tenEl = document.getElementById('name').value ; 
    var letterTen = new RegExp("^[A-Z]{1,}") ; 
    if (letterTen.test(tenEl)) {
        document.getElementById('tbTen').innerHTML = "" ; 
        flag = true ; 
    }else if( tenEl == "") {
        document.getElementById('tbTen').innerHTML ="Vui lòng không để trống" ; 
        flag = false ; 
    }
    else {
        document.getElementById('tbTen').innerHTML = "Tài khoản phải là chữ.Vui lòng nhập lại" ;
        flag = false ;  
    }
    return flag ;
}

function kiemTraEmail() {
    var flag = true ;
    var emailEl = document.getElementById('email').value ; 
    var letterEmail = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
    if(letterEmail.test(emailEl)) {
        document.getElementById('tbEmail').innerHTML = "" ;
        flag = true ; 
    }else if(emailEl == "") {
        document.getElementById('tbEmail').innerHTML ="Vui lòng không để trống" ; 
        flag = false ; 
    }
    else {
        document.getElementById('tbEmail').innerHTML = "Email không hợp lệ. Vui lòng theo form sau: abc@def.gh" ;
        flag = false ; 
    }
    return flag ;
}

function kiemTraMatKhau() {
    var flag = true ; 
    var matKhaulEl = document.getElementById('password').value ; 
    var letterMatKhau = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{6,10}$/;
    if(letterMatKhau.test(matKhaulEl)) {
        document.getElementById('tbMatKhau').innerHTML = "" ;
        flag = true ; 
    }else if(matKhaulEl == "") {
        document.getElementById('tbMatKhau').innerHTML ="Vui lòng không để trống" ; 
        flag = false ; 
    }
    else {
        document.getElementById('tbMatKhau').innerHTML = "Mật khẩu không hợp lệ.Mật khẩu gồm 1 ký tự đặc biệt , 1 ký tự in hoa và 1 chữ số" ;
        flag = false ; 
    }
    return flag ;
}

function kiemTraChucVu() {
    var flag = true ;
    var chucVuEl = document.getElementById('chucvu').value ; 
    if (chucVuEl === "Chọn chức vụ") {
        document.getElementById('tbChucVu').innerHTML = "Xin vui lòng chọn chức vụ" ; 
        flag = false ; 
        
    }else {
        document.getElementById('tbChucVu').innerHTML = "" ; 
        flag = true ; 
    }
    return flag ;
    
}

function kiemTraNgayLam() {
    var flag = true ; 
    var ngayLamEl = document.getElementById('datepicker').value ; 
    var letterNgayLam = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/ ; 
    if (letterNgayLam.test(ngayLamEl)) {
        document.getElementById('tbNgay').innerHTML = "" ; 
        flag = true ; 
    }else if( ngayLamEl == "") {
        document.getElementById('tbNgay').innerHTML ="Vui lòng không để trống" ; 
        flag = false ; 
    }
    else {
        document.getElementById('tbNgay').innerHTML = "Ngày làm không hợp lệ . Vui lòng nhập lại" ; 
        flag = false ; 
    }
    return flag ;
}

function kiemTraLuong() {
    var flag = true ; 
    var luongEL = document.getElementById('luongCB').value * 1 ; 
    if (luongEL >= 1000000 && luongEL <= 20000000) {
        document.getElementById('tbLuongCB').innerHTML = "" ; 
        flag = true ; 
    }else {
        document.getElementById('tbLuongCB').innerHTML = "Lương nằm trong khoảng 1tr-20tr" ; 
        flag = false ; 
    }
    return flag ;
}

function kiemTraGioLam() {
    var flag = true ; 
    var gioLamEL = document.getElementById('gioLam').value * 1 ; 
    if (gioLamEL >= 80 && gioLamEL <= 200) {
        document.getElementById('tbGiolam').innerHTML = "" ; 
        flag = true ; 
    }else {
        document.getElementById('tbGiolam').innerHTML = "Giờ làm nằm trong khoảng 80-200 giờ" ; 
        flag = false ;
    }
    return flag ;     
}
