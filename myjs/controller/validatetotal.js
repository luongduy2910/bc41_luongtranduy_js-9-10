function validateform() {
    var flag = true ; 
    var tk = document.getElementById('tknv').value ; 
    var letterTk = new RegExp("^[a-z0-9_-]{4,6}$") ; 
    if(letterTk.test(tk)) {
        document.getElementById('tbTKNV').innerHTML = "" ; 
        flag = true ; 
    }else {
        document.getElementById('tbTKNV').innerHTML = "Tài khoản từ 4 đến 6 ký tự .Vui lòng nhập lại" ; 
        flag = false ; 
    } 
    var tenEl = document.getElementById('name').value ; 
    var letterTen = new RegExp("^[A-Z]{1,}") ; 
    if (letterTen.test(tenEl)) {
        document.getElementById('tbTen').innerHTML = "" ; 
        flag = true ; 
    }else {
        document.getElementById('tbTen').innerHTML = "Tài khoản phải là chữ.Vui lòng nhập lại" ;
        flag = false ;  
    }
    var emailEl = document.getElementById('email').value ; 
    var letterEmail = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
    if(letterEmail.test(emailEl)) {
        document.getElementById('tbEmail').innerHTML = "" ;
        flag = true ; 
    }else {
        document.getElementById('tbEmail').innerHTML = "Email không hợp lệ. Vui lòng theo form sau: abc@def.gh" ;
        flag = false ; 
    }
    var matKhaulEl = document.getElementById('password').value ; 
    var letterMatKhau = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{6,10}$/;
    if(letterMatKhau.test(matKhaulEl)) {
        document.getElementById('tbMatKhau').innerHTML = "" ;
        flag = true ; 
    }else {
        document.getElementById('tbMatKhau').innerHTML = "Mật khẩu không hợp lệ.Mật khẩu gồm 1 ký tự đặc biệt , 1 ký tự in hoa và 1 chữ số" ;
        flag = false ; 
    }
    var chucVuEl = document.getElementById('chucvu').value ; 
    if (chucVuEl === "Chọn chức vụ") {
        document.getElementById('tbChucVu').innerHTML = "Xin vui lòng chọn chức vụ" ; 
        flag = false ; 
        
    } else {
        document.getElementById('tbChucVu').innerHTML = "" ; 
        flag = true ; 
    }
    var ngayLamEl = document.getElementById('datepicker').value ; 
    var letterNgayLam = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/ ; 
    if (letterNgayLam.test(ngayLamEl)) {
        document.getElementById('tbNgay').innerHTML = "" ; 
        flag = true ; 
    }else {
        document.getElementById('tbNgay').innerHTML = "Ngày làm không hợp lệ . Vui lòng nhập lại" ; 
        flag = false ; 
    }
    var luongEL = document.getElementById('luongCB').value * 1 ; 
    if (luongEL >= 1000000 && luongEL <= 20000000) {
        document.getElementById('tbLuongCB').innerHTML = "" ; 
        flag = true ; 
    }else {
        document.getElementById('tbLuongCB').innerHTML = "Lương nằm trong khoảng 1tr-20tr" ; 
        flag = false ; 
    }
    var gioLamEL = document.getElementById('gioLam').value * 1 ; 
    if (gioLamEL >= 80 && gioLamEL <= 200) {
        document.getElementById('tbGiolam').innerHTML = "" ; 
        flag = true ; 
    }else {
        document.getElementById('tbGiolam').innerHTML = "Giờ làm nằm trong khoảng 80-200 giờ" ; 
        flag = false ; 
    }
    return flag ;             
}