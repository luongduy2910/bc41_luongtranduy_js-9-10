function layThongTinTuForm() {

        // nếu kiểm tra các ô input hợp lệ thì sẽ bắt đầu lấy dữ liệu 
        var _taikhoan = document.getElementById('tknv').value ; 
        var _hovaTen = document.getElementById('name').value ; 
        var _email = document.getElementById('email').value ;
        var _matKhau = document.getElementById('password').value ; 
        var _ngayLam = document.getElementById('datepicker').value ; 
        var _luongCB = document.getElementById('luongCB').value * 1  ; 
        var _chucVu = document.getElementById('chucvu').value ; 
        var _gioLam = document.getElementById('gioLam').value * 1  ;  
        // tạo object 
        var nv = new NhanVien (_taikhoan ,
            _hovaTen ,
            _email ,
            _matKhau ,
            _ngayLam ,
            _luongCB ,
            _chucVu ,
            _gioLam)
            return nv ;         
}

function renderNhanVien(arr) {
    var contentTable = "" ; 
    for (var i = 0 ; i < arr.length ; i++) {
        var current = arr[i] ; 
        contentTr = 
        `
        <tr>
            <td>${current.taikhoan}</td>
            <td>${current.hovaTen}</td>
            <td>${current.email}</td>
            <td>${current.ngayLam}</td>
            <td>${current.chucVu}</td>
            <td>${current.tinhLuong()}</td>
            <td>${current.xepLoai()}</td>
            <td><button onclick = "xoaNV(${i})" class = "btn btn-danger mr-1">Delete</button><button onclick = "capNhatNV(${i})" class = "btn btn-primary">Edit</button></td>
        </tr>
        `
        contentTable += contentTr ; 
    }
    document.getElementById('tableDanhSach').innerHTML = contentTable ; 
    
}



function lamMoiForm() {
    document.getElementById('tknv').value = "" ; 
    document.getElementById('name').value = "" ; 
    document.getElementById('email').value = "" ;
    document.getElementById('password').value = "" ; 
    document.getElementById('luongCB').value = "" ; 
    document.getElementById('gioLam').value = "" ;  
    
}
