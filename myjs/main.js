/**
 * viết chương trình CRUD , gọi nôm na là thêm-sửa-xóa 
 * CRUD : 
 * C : tạo chương trình (create)
 * R : đọc chương trình (read && render)
 * U: cập nhập dữ liệu (update)
 * D: xóa dữ liệu (delete)
 */

var DSNV = [] ; 
function themNhanVien() {
    // C: tạo chương trình 
    var isValid = true ; 
    isValid = kiemTraDoDai(4 , 6) 
    && kiemTraSo() 
    && kiemTraTrung(DSNV) ;
    isValid = isValid & kiemTraTen() & kiemTraChucVu() & kiemTraEmail() & kiemTraGioLam() & kiemTraMatKhau() & kiemTraLuong() & kiemTraNgayLam() ; 
    if(isValid) {
        var nv = layThongTinTuForm() ; 
        DSNV.push(nv) ; 
        // Lưu DSNV xuống localStorage 
        localStorage.setItem('local_DSNV' , JSON.stringify(DSNV)) ; 
        renderNhanVien(DSNV) ; 
        lamMoiForm() ;      
    }
    
}

var dsnvjson = localStorage.getItem('local_DSNV') ; 

if (dsnvjson != null) {
    var dataArr = JSON.parse(dsnvjson) ; 
    DSNV = dataArr.map(function(item){
        var nv = new NhanVien(item.taikhoan , item.hovaTen , item.email , item.matKhau , item.ngayLam , item.luongCB , item.chucVu , item.gioLam) ; 
        return nv ; 
    })
    renderNhanVien(DSNV) ; 
}


function xoaNV(index) {
    DSNV.splice(index , 1) ; 
    localStorage.setItem('local_DSNV' , JSON.stringify(DSNV)) ; 
    renderNhanVien(DSNV) ; 
}

function capNhatNV(index) {
    var modalEl = document.getElementById("myModal") ; 
    modalEl.style.display = "block" ; 
    modalEl.classList.add('show') ; 
    document.getElementById('tknv').disabled = true ; 
    document.getElementById('tknv').value = DSNV[index].taikhoan ; 
    document.getElementById('name').value = DSNV[index].hovaTen ; 
    document.getElementById('email').value = DSNV[index].email ;
    document.getElementById('password').value = DSNV[index].matKhau ; 
    document.getElementById('datepicker').value = DSNV[index].ngayLam ; 
    document.getElementById('luongCB').value = DSNV[index].luongCB; 
    document.getElementById('chucvu').value = DSNV[index].chucVu ; 
    document.getElementById('gioLam').value = DSNV[index].gioLam ;  
    document.getElementById('index').value = index ; 
    
}

function suaThongTin() {
    var index = document.getElementById('index').value ; 
    var isValid = true ; 
    isValid = isValid & kiemTraTen() & kiemTraChucVu() & kiemTraEmail() & kiemTraGioLam() & kiemTraMatKhau() & kiemTraLuong() & kiemTraNgayLam() ; 
    if (isValid) {
        DSNV[index] = layThongTinTuForm() ; 
        localStorage.setItem('local_DSNV' , JSON.stringify(DSNV)) ; 
        renderNhanVien(DSNV) ; 
        var modalEl = document.getElementById("myModal") ; 
        modalEl.style.display = "none" ; 
        modalEl.classList.remove('show') ;
        lamMoiForm() ;  
    }
    document.getElementById('tknv').disabled = false ; 
}

function TimKiemNhanVien() {
    var xepLoaiNV = document.getElementById('searchName').value ; 
    var NVCT = [] ; 
    for (var index = 0; index < DSNV.length; index++) {
        var element = DSNV[index];
        if (element.xepLoai() == xepLoaiNV) {
            NVCT.push(element) ; 
        }
        renderNhanVien(NVCT) ; 
    }
    // renderNhanVien(NVCT) ; 
}
